#define CATCH_CONFIG_MAIN

#include "SVF-FE/LLVMUtil.h"
#include "WPA/Andersen.h"
#include <ComplexityPass.hpp>
#include <IntervalAnalysis.hpp>
#include <catch2/catch.hpp>
#include <iostream>
#include <unordered_set>

using namespace boost::numeric;

/*

TEST_CASE("Program State Equality", "[interval_analysis]") {
  ProgramState f;
  ProgramState s;
  SymID key = 1;
  interval<uint64_t> test = 12;
  AbstractValue simple_int = AbstractValue::create_u64(test);
  REQUIRE(f == s);
}

TEST_CASE("Handle Function Entry Block", "[interval_analysis]") {
  std::vector<std::string> moduleNameVec{"../../examples/simple_loop.ll"};
  SVFModule *svfModule =
      LLVMModuleSet::getLLVMModuleSet()->buildSVFModule(moduleNameVec);

  FlowSensitive *fpta = FlowSensitive::createFSWPA(svfModule);

  std::cout << svfModule->getModuleIdentifier() << std::endl;

  // grab entry function'

  const Function *target = nullptr;
  for (auto it : *svfModule) {
    const Function *fun = it;
    if (fun->getName().str() == "loop_until") {
      target = fun;
    }
  }

  REQUIRE(target != nullptr);

  IntervalAnalysis test(*fpta, &target->getEntryBlock(), target);
  ProgramState f;
  PAG *pag = fpta->getPAG();
  ICFG *icfg = pag->getICFG();
  const FunEntryBlockNode *ent_node = icfg->getFunEntryICFGNode(target);

  for (auto node_prev : ent_node->getInEdges()) {
    const CallBlockNode *cb =
        llvm::cast<CallBlockNode>(node_prev->getSrcNode());

    for (auto arg : cb->getActualParms()) {
      // arg->getValue()->print(llvm::outs());
      if (arg->getValueName().rfind("conv") == std::string::npos) {
        std::cout << "writing register" << std::endl;
        f = f.insert_register(arg->getValue(),
                              AbstractValue::create_u64(1000000));
      } else {

        f = f.insert_register(arg->getValue(), AbstractValue::create_u64(10));
      }
    }
  }

  BlockState res = f.apply_entry_block(ent_node);
  NodeID one_out = (*ent_node->getOutEdges().begin())->getDstID();
  // std::cout << "t: " << one_out << std::endl;
  ProgramState one = res.get_state(one_out);

  const ProgramState &res_state = one;
  auto param = (*ent_node->getFormalParms().begin())->getValue();
  auto reg = *res_state.get_register_as_u64(param);

  REQUIRE(reg.lower() == 10);
  REQUIRE(reg.upper() == 1000000);
  // FlowSensitive::releaseFSWPA();
}

TEST_CASE("Handle Ret Block", "[interval_analysis]") {
  std::vector<std::string> moduleNameVec{"../../examples/simple_loop.ll"};

  // grab entry function'

  FlowSensitive *fpta = FlowSensitive::createFSWPA(nullptr);
  const Function *target = nullptr;
  const Function *retfrom = nullptr;
  for (auto it : *fpta->getModule()) {
    const Function *fun = it;
    if (fun->getName().str() == "expensive_function") {
      target = fun;
    } else if (fun->getName().str() == "loop_until") {
      retfrom = fun;
    }
  }

  REQUIRE(target != nullptr);
  REQUIRE(retfrom != nullptr);
  IntervalAnalysis test(*fpta, &target->getEntryBlock(), target);
  ProgramState f;
  PAG *pag = fpta->getPAG();
  ICFG *icfg = pag->getICFG();

  const RetBlockNode *ret_node = nullptr;
  const Value *call_value = nullptr;

  for (const auto &bb : *target) {
    for (const auto &insn : bb) {
      if (insn.getOpcode() == Instruction::Call) {
        ret_node = icfg->getRetBlockNode(&insn);
        call_value = &insn;
      }
    }
  }

  REQUIRE(ret_node != nullptr);

  boost::numeric::interval<uint64_t> in(0, 10);
  auto av_in = AbstractValue::create_u64(in);

  const Value *ret_value = nullptr;
  for (const auto &bb : *retfrom) {
    for (const auto &insn : bb) {
      if (insn.getOpcode() == Instruction::Ret) {
        ret_value = insn.getOperand(0);
      }
    }
  }

  REQUIRE(ret_value != nullptr);

  f = f.insert_register(ret_value, av_in);

  auto res = f.apply_ret_block(ret_node);

  auto nxt = *ret_node->OutEdgeBegin();
  auto nxt_id = nxt->getDstID();
  auto res_prog_state = res.get_state(nxt_id);
  auto reg_val = *res_prog_state.get_register_as_u64(call_value);
  REQUIRE(reg_val.lower() == 0);
  REQUIRE(reg_val.upper() == 10);
}

TEST_CASE("Handle Store Instruction", "[interval_analysis]") {
  std::vector<std::string> moduleNameVec{"../../examples/simple_loop.ll"};

  // grab entry function'

  FlowSensitive *fpta = FlowSensitive::createFSWPA(nullptr);
  ProgramState f;
  PAG *pag = fpta->getPAG();
  ICFG *icfg = pag->getICFG();

  const Function *target = nullptr;
  // const Function *target = nullptr;
  for (auto it : *fpta->getModule()) {
    const Function *fun = it;
    if (fun->getName().str() == "expensive_function") {
      // target = fun;
    } else if (fun->getName().str() == "loop_until") {
      target = fun;
    }
  }
  REQUIRE(target);

  const llvm::Instruction *store_insn = nullptr;
  for (const auto &insn : target->getEntryBlock()) {
    if (insn.getOpcode() == llvm::Instruction::MemoryOps::Store) {
      store_insn = &insn;
    }
  }

  REQUIRE(store_insn);

  auto block_node = icfg->getIntraBlockNode(store_insn);
  REQUIRE(block_node);

  // f.transfer_function(block_node, *fpta);
  FlowSensitive::releaseFSWPA();
}*/

/*
TEST_CASE("Handle Store Instruction Struct", "[interval_analysis]") {
  std::vector<std::string> moduleNameVec{"../../examples/loop_phi.ll"};

  SVFModule *svfModule =
      LLVMModuleSet::getLLVMModuleSet()->buildSVFModule(moduleNameVec);
  // grab entry function'

  FlowSensitive *fpta = FlowSensitive::createFSWPA(svfModule);

  ProgramState f;
  PAG *pag = fpta->getPAG();
  ICFG *icfg = pag->getICFG();

  const Function *entry = nullptr;
  const Function *target = nullptr;
  for (auto it : *fpta->getModule()) {
    const Function *fun = it;
    if (fun->getName().str() == "func") {
      entry = fun;
    } else if (fun->getName().str() == "loop_until") {
      target = fun;
    }
  }

  REQUIRE(entry);
  REQUIRE(target);

  const BasicBlock *zeroer = nullptr;
  const BasicBlock *counter = nullptr;
  for (const auto &bb : *target) {
    if (bb.getName().str().find("if.end") != std::string::npos) {
      zeroer = &bb;
      counter = bb.getSingleSuccessor();
    }
  }

  // node 33 and 34 join is failing

  REQUIRE(zeroer);
  REQUIRE(counter);

  IntervalAnalysis ai(*fpta, counter, zeroer, entry);

  auto inter = ai.get_loop_count().as_u64()->get_value();
  std::cout << "[" << inter.lower() << "," << inter.upper() << "]" << std::endl;

  FlowSensitive::releaseFSWPA();
}
*/

TEST_CASE("Complexity Pass Test", "[comp_pass]") {
  std::vector<std::string> moduleNameVec{"../../examples/loop_phi.ll"};

  SVFModule *svfModule =
      LLVMModuleSet::getLLVMModuleSet()->buildSVFModule(moduleNameVec);
  // grab entry function'

  FlowSensitive *fpta = FlowSensitive::createFSWPA(svfModule);

  ProgramState f;
  PAG *pag = fpta->getPAG();
  ICFG *icfg = pag->getICFG();

  const Function *entry = nullptr;
  for (auto it : *fpta->getModule()) {
    const Function *fun = it;
    if (fun->getName().str() == "func") {
      entry = fun;
    }
  }

  REQUIRE(entry);

  const Instruction *call_expensive = nullptr;
  const Instruction *ret = nullptr;

  for (const auto &bb : *entry) {
    for (const auto &insn : bb) {
      if (insn.getOpcode() == Instruction::Call) {
        call_expensive = &insn;
      } else if (insn.getOpcode() == Instruction::Ret) {
        ret = &insn;
      }
    }
  }

  auto *tnode = icfg->getFunEntryICFGNode(entry);
  std::vector<NodeID> stk;
  stk.push_back(tnode->getId());
  std::cout << std::to_string(tnode->getId()) << std::endl;
  std::unordered_set<NodeID> seen;
  size_t total_size = 0;
  while (!stk.empty()) {
    auto next = stk.back();
    stk.pop_back();

    if (seen.find(next) != seen.end()) {
      continue;
    }

    seen.insert(next);

    total_size++;
    for (auto child : icfg->getICFGNode(next)->getOutEdges()) {
      // std::cout << std::to_string(child->getDstID()) << std::endl;
      auto *seen_node = icfg->getICFGNode(child->getDstID());
      if (auto *nd = llvm::dyn_cast<CallBlockNode>(seen_node)) {

        if (nd->getCallSite().getInstruction() == call_expensive) {
          std::cout << "Found yay" << std::endl;
          std::cout << "yay? " << child->getDstID() << std::endl;
        }
      }
      if (seen.find(child->getDstID()) == seen.end()) {
        stk.push_back(child->getDstID());
      }
    }
  }

  std::cout << "Seen: " << std::to_string(total_size) << std::endl;
  std::cout << std::to_string(
                   icfg->getBlockICFGNode(call_expensive)->getOutEdges().size())
            << std::endl;

  REQUIRE(call_expensive);
  REQUIRE(ret);

  // REQUIRE(!icfg->getCallBlockNode(call_expensive)->getOutEdges().empty());

  // REQUIRE(!icfg->getIntraBlockNode(call_expensive)->getOutEdges().empty());

  ComplexityPass compass(*fpta, entry, ret);
  auto res = compass.calculate_complexity(call_expensive);
  // REQUIRE(res);

  REQUIRE(res.is_initialized());
  auto value = *res;

  std::cout << "[" << value.lower() << "," << value.upper() << "]" << std::endl;
}