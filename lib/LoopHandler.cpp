#include <ComplexityPass.hpp>
#include <IntervalAnalysis.hpp>
#include <LoopHandler.hpp>
#include <functional>
#include <gsl/gsl_assert>

using namespace boost::numeric;

auto LoopHandler::is_loop_head(const BasicBlock *block) const -> bool {
  auto curr_loop_info = this->delegate.getLoopInfo(block->getParent());
  return curr_loop_info->isLoopHeader(block);
}

auto LoopHandler::calculate_loop_count(const BasicBlock *header,
                                       const FlowSensitive &fpta,
                                       const Function *entry_point) const
    -> IntervalAbstract<uint64_t> {

  Expects(this->is_loop_head(header));

  if (this->cached_counts.find(header) != this->cached_counts.end()) {
    return this->cached_counts[header];
  }

  auto lopi = this->delegate.getLoopInfo(header->getParent());
  auto inner_loop = lopi->getLoopFor(header);
  auto loop_pred = inner_loop->getLoopPredecessor();

  IntervalAnalysis ai(fpta, header, loop_pred, entry_point);
  return ai.get_loop_count().as_u64()->get_value();
}

auto LoopHandler::get_loop_cost(NodeID block, const ComplexityPass &passmanager,
                                ICFG *icfg) const
    -> IntervalAbstract<uint64_t> {
  auto bbnode = icfg->getICFGNode(block)->getBB();
  Expects(this->is_loop_head(bbnode));
  auto exits = this->get_loop_exits(bbnode);

  IntervalAbstract<uint64_t> fixed_loop_count = this->calculate_loop_count(
      bbnode, passmanager.fpa, passmanager.entry_func);

  std::unordered_set<NodeID> exit_set;
  for (auto s : exits) {
    auto first_id = icfg->getIntraBlockICFGNode(&*s->begin())->getId();
    exit_set.insert(first_id);
  }

  auto sub_comp = passmanager.calculate_complexity(block, exit_set);
  if (sub_comp) {
    return fixed_loop_count * *sub_comp;
  }
  return 0;
}
auto LoopHandler::get_loop_exits(const BasicBlock *block) const
    -> BlockCollection {
  auto curr_loop_info = this->delegate.getLoopInfo(block->getParent());
  auto loop = curr_loop_info->getLoopFor(block);
  Expects(loop != nullptr);
  BlockCollection vec;
  loop->getExitBlocks(vec);
  return vec;
}