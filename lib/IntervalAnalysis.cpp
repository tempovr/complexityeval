#include "llvm/IR/InstVisitor.h"
#include "llvm/Support/Casting.h"
#include <IntervalAnalysis.hpp>
#include <functional>
#include <queue>
#include <unordered_set>

IntervalAnalysis::IntervalAnalysis(const FlowSensitive &fpta,
                                   gsl::not_null<const BasicBlock *> counter,
                                   gsl::not_null<const BasicBlock *> zeroer,
                                   gsl::not_null<const Function *> entry_point)
    : fpta(fpta), entry_point(entry_point) {
  const Instruction *insn_counter = &*(counter->begin());
  const Instruction *insn_zeroer = &*(zeroer->begin());

  this->counter_block =
      this->fpta.getPAG()->getICFG()->getBlockICFGNode(insn_counter)->getId();
  this->zeroer_block =
      this->fpta.getPAG()->getICFG()->getBlockICFGNode(insn_zeroer)->getId();
  this->build_analysis();
}

#include <iostream>

auto postorder_weights(ICFG *icfg, NodeID start)
    -> std::unordered_map<NodeID, size_t> {
  // two stack iterative postorder
  std::vector<NodeID> temp_stack;
  std::vector<NodeID> final_order;
  std::unordered_set<NodeID> seen;
  temp_stack.push_back(start);

  while (!temp_stack.empty()) {
    auto curr = temp_stack.back();
    temp_stack.pop_back();

    if (seen.find(curr) == seen.end()) {
      seen.insert(curr);
    } else {
      continue;
    }

    ICFGNode *nd = icfg->getICFGNode(curr);
    for (auto out_edge : nd->getOutEdges()) {
      if (seen.find(out_edge->getDstID()) == seen.end()) {
        temp_stack.push_back(out_edge->getDstID());
      }
    }

    final_order.push_back(curr);
  }

  std::unordered_map<NodeID, uint64_t> node_to_order;

  for (size_t ind = final_order.size() - 1, ordering = 0;
       ordering < final_order.size(); ind--, ordering++) {

    NodeID curr_id = final_order[ind];
    node_to_order.insert({curr_id, ordering});
  }

  return node_to_order;
}

namespace {
struct NodeIDCompare {
public:
  NodeIDCompare(const std::unordered_map<NodeID, uint64_t> &node_to_order)
      : node_to_order(node_to_order) {}

  auto operator()(NodeID a, NodeID b) -> bool {
    uint64_t a_ord = this->node_to_order.find(a)->second;
    uint64_t b_ord = this->node_to_order.find(b)->second;
    return a_ord > b_ord;
  }

private:
  const std::unordered_map<NodeID, uint64_t> &node_to_order;
};
} // namespace
void IntervalAnalysis::build_analysis() {

  ICFG *icfg = this->fpta.getPAG()->getICFG();
  const auto *entry_icfg_node = icfg->getFunEntryICFGNode(this->entry_point);
  NodeID entry_node = entry_icfg_node->getId();
  auto node_to_order =
      postorder_weights(icfg, entry_node); // smaller numbers earlier

  std::priority_queue<NodeID, std::vector<NodeID>, NodeIDCompare> worklist(
      node_to_order);
  worklist.push(entry_node);

  while (!worklist.empty()) {
    NodeID curr_node = worklist.top();
    worklist.pop();

    if (this->execute_block(curr_node, icfg)) {

      ICFGNode *nd = icfg->getICFGNode(curr_node);
      for (auto child_edge : nd->getOutEdges()) {
        worklist.push(child_edge->getDstID());
      }
    }
  }
}

auto IntervalAnalysis::get_loop_count() const -> AbstractValue {
  return this->values.find(this->counter_block)->second.get_counter();
}

AbstractValue::AbstractValue() : delegate(Bottom()) {}

auto IntervalAnalysis::get_join(NodeID blk, ICFG *graph) -> ProgramState {
  ProgramState total;
  auto *node = graph->getICFGNode(blk);
  for (auto edge : node->getInEdges()) {
    auto val = this->get_state(edge->getSrcID());
    auto join_with = val.get_state(blk);
    total = total.join(join_with);
  }
  return total;
}

//-----SAFE -- why did I make this comment? is it safe? why is it safe? why was
// I concerned
auto IntervalAnalysis::execute_block(NodeID blk, ICFG *graph) -> bool {
  ProgramState prev = this->get_join(blk, graph);
  auto graph_node = graph->getICFGNode(blk);

  boost::optional<BlockState> final;

  if (this->counter_block == blk) {
    prev = prev.increment_counter();
  } else if (this->zeroer_block == blk) {
    prev = prev.zero_counter();
  }

  if (llvm::isa<IntraBlockNode>(graph_node)) {
    auto boring_node = llvm::cast<IntraBlockNode>(
        graph_node); // if you were wondering this is
                     // a technical term from VexIR for a jump to the
                     // subsequent instruction

    final = prev.transfer_function(boring_node, this->fpta);
  } else if (llvm::isa<CallBlockNode>(graph_node)) {
    final = prev.copy_self_to_all(graph_node);
  } else if (llvm::isa<FunEntryBlockNode>(graph_node)) {
    auto entry_block = llvm::cast<FunEntryBlockNode>(graph_node);
    final = prev.apply_entry_block(entry_block);
  } else if (llvm::isa<FunExitBlockNode>(graph_node)) {
    final = prev.copy_self_to_all(graph_node);
  } else {
    Expects(llvm::isa<RetBlockNode>(graph_node));
    auto ret_block = llvm::cast<RetBlockNode>(graph_node);
    final = prev.apply_ret_block(ret_block);
  }

  if (this->values.find(blk) != this->values.end()) {
    final = final->widen(this->values.find(blk)->second);
  }

  if (this->values.find(blk) == this->values.end() ||
      this->values.find(blk)->second != final) {
    this->values[blk] = std::move(*final);
    return true;
  }

  return false;
}

auto BlockState::operator==(const BlockState &rhs) const -> bool {
  return this->succ_to_state == rhs.succ_to_state;
}

auto BlockState::operator!=(const BlockState &rhs) const -> bool {
  return !(*this == rhs);
}

#include <algorithm>
#include <iostream>
#include <vector>

auto ProgramState::copy_self_to_all(const ICFGNode *blk) const -> BlockState {
  std::unordered_map<NodeID, ProgramState> res;

  std::transform(blk->OutEdgeBegin(), blk->OutEdgeEnd(),
                 std::inserter(res, res.end()), [this](const ICFGEdge *edge) {
                   return std::make_pair(edge->getDstID(), *this);
                 });

  return BlockState(std::move(res));
}

auto ProgramState::apply_ret_block(const RetBlockNode *ret_block) const
    -> BlockState {
  auto pag_ret = ret_block->getActualRet();

  if (!pag_ret) {
    return this->copy_self_to_all(ret_block);
  }

  std::vector<const Value *> ret_vals;
  ret_vals.reserve(pag_ret->getInEdges().size());

  std::transform(
      pag_ret->InEdgeBegin(), pag_ret->InEdgeEnd(),
      std::back_inserter(ret_vals), [](const PAGEdge *edge) {
        return edge->getSrcNode()
            ->InEdgeBegin()
            .
            operator*()
            ->getSrcNode()
            ->getValue(); // TODO this only works if there is only one ret node
                          // from a function(which llvm guarentees ) we should
                          // just be nicer about it
      });
  auto val_iter = ret_vals.begin();

  AbstractValue joined;
  while (val_iter != ret_vals.end()) {
    auto curr_val = *val_iter;

    joined = joined.join(this->val_to_aval(curr_val));

    val_iter++;
  }

  ProgramState n_state = *this;

  return n_state.insert_register(pag_ret->getValue(), joined)
      .copy_self_to_all(ret_block);
}

auto ProgramState::apply_entry_block(const FunEntryBlockNode *entry_block) const
    -> BlockState {

  std::vector<const CallBlockNode *> calls;
  calls.reserve(entry_block->getOutEdges().size());

  std::transform(entry_block->InEdgeBegin(), entry_block->InEdgeEnd(),
                 std::back_inserter(calls), [](const ICFGEdge *val) {
                   return llvm::cast<CallBlockNode>(val->getSrcNode());
                 });

  size_t param_num = 0;
  ProgramState new_state = *this;
  for (auto param : entry_block->getFormalParms()) {
    AbstractValue new_param_value;
    for (const auto &call_block : calls) {
      auto passed_args = call_block->getActualParms();
      Expects(passed_args.size() == entry_block->getFormalParms().size());
      auto curr_arg = passed_args[param_num];
      auto curr_arg_value = curr_arg->getValue();

      auto prev_value = this->val_to_aval(curr_arg_value);
      new_param_value = new_param_value.join(prev_value);
    }
    param_num++;

    new_state = new_state.insert_register(param->getValue(), new_param_value);
  }

  return new_state.copy_self_to_all(entry_block);
}

auto ProgramState::operator==(const ProgramState &rhs) const -> bool {
  return this->alocs == rhs.alocs && this->registers == rhs.registers;
}

auto ProgramState::operator!=(const ProgramState &rhs) const -> bool {
  return !(*this == rhs);
}

auto ProgramState::join(const ProgramState &rhs) const -> ProgramState {
  auto new_reg = rhs.registers.join(this->registers);
  auto new_mem = rhs.alocs.join(this->alocs);
  auto new_counter = rhs.loop_counter.join(this->loop_counter);
  return ProgramState(new_reg, new_mem, new_counter);
}

namespace {

class TruncVisitor : boost::static_visitor<AbstractValue> {
public:
  TruncVisitor(const llvm::Type *ty) : to_type(ty) {}
  auto operator()(const Bottom &i1) const -> AbstractValue {
    return AbstractValue(Bottom());
  }

  auto operator()(const ComparisonResult &i1) const -> AbstractValue {
    throw std::runtime_error("trying to truncate a bool");
  }

  auto operator()(const Convertable &i1) const -> AbstractValue {
    if (const auto int_type =
            llvm::dyn_cast<llvm::IntegerType>(this->to_type)) {
      if (int_type->getSignBit() != 0) {
        switch (int_type->getBitWidth()) {
        case 64:
          return i1.trunc_i64();
        case 32:
          return i1.trunc_i32();
        default:
          throw std::runtime_error("Trunc type error");
        }
      } else {
        switch (int_type->getBitWidth()) {
        case 64:
          return i1.trunc_u64();
        case 32:
          return i1.trunc_u32();
        default:
          throw std::runtime_error("Trunc type error");
        }
      }
    } else {
      throw std::runtime_error("Trunc type error");
    }
  }

private:
  const llvm::Type *to_type;
};

class ComparisonVisitor : boost::static_visitor<AbstractValue> {
public:
  ComparisonVisitor(llvm::CmpInst::Predicate pred, const Value *addr_first,
                    const Value *addr_second)
      : pred(pred), addr_self(addr_first), addr_other(addr_second) {}
  template <typename T>
  auto operator()(const T &i1, const T &i2) const -> AbstractValue {
    switch (pred) {
    case llvm::CmpInst::Predicate::ICMP_EQ:
      return i1.eq(i2, this->addr_self, this->addr_other);
    case llvm::CmpInst::Predicate::ICMP_SLT:
      return i1.lt(i2, this->addr_self, this->addr_other);
    default:
      throw std::runtime_error("Unsupported comparison");
    }
  }

  template <typename T>
  auto operator()(const T &i1, const Bottom &i2) const -> AbstractValue {
    return AbstractValue(Bottom());
  }

  template <typename T>
  auto operator()(const Bottom &i1, const T &i2) const -> AbstractValue {
    return AbstractValue(Bottom());
  }

  auto operator()(const Bottom &i1, const Bottom &i2) const -> AbstractValue {
    return AbstractValue(Bottom());
  }

  template <typename T, typename U>
  auto operator()(const T &i1, const U &i2) const -> AbstractValue {
    throw std::runtime_error("Cant compare differing types");
  }

  auto operator()(const ComparisonResult &i1, const ComparisonResult &i2) const
      -> AbstractValue {
    throw std::runtime_error("Cant compare to a result of a compare");
  }

private:
  llvm::CmpInst::Predicate pred;
  const Value *addr_self;
  const Value *addr_other;
};

class TransFunctionVisitor
    : public llvm::InstVisitor<TransFunctionVisitor, BlockState> {
private:
  const PointsTo &get_pts(NodeID id) {
    return const_cast<FlowSensitive &>(this->flow_results).getPts(id);
  }

  auto value_to_aval(const Value *val) const -> AbstractValue {
    if (const auto cst = llvm::dyn_cast<llvm::Constant>(val)) {
      return AbstractValue(cst);
    } else {
      return this->start_state.get_register(val);
    }
  }

  auto handle_ptr_assign_insn() const -> BlockState {
    auto next = this->start_state.insert_register(
        this->intra_node->getInst(), // TODO there should be a type to represent
                                     // that we can perform no operations on
                                     // this type, it is not bottom
        AbstractValue(Bottom()));
    return next.copy_self_to_all(this->intra_node);
  }

public:
  TransFunctionVisitor(const IntraBlockNode *intra_node, ProgramState state,
                       const FlowSensitive &fpta)
      : intra_node(intra_node), start_state(std::move(state)),
        flow_results(fpta) {}

  BlockState visitAdd(BinaryOperator &I) {
    auto first_op = this->value_to_aval(I.getOperand(0));
    auto second_op = this->value_to_aval(I.getOperand(1));

    auto added = first_op.add(second_op);
    auto res =
        this->start_state.insert_register(this->intra_node->getInst(), added);
    return res.copy_self_to_all(this->intra_node);
  }
  BlockState visitBinaryOperator(BinaryOperator &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitICmpInst(CmpInst &I) {
    auto first_op = this->value_to_aval(I.getOperand(0));
    auto second_op = this->value_to_aval(I.getOperand(1));
    auto comp_res = first_op.compare(second_op, I.getPredicate(),
                                     I.getOperand(0), I.getOperand(1));
    auto temp = this->start_state.insert_register(this->intra_node->getInst(),
                                                  comp_res);
    return temp.copy_self_to_all(this->intra_node);
  }

  BlockState visitBranchInst(BranchInst &I) {
    if (I.isUnconditional()) {
      // if it is a direct jump take care of it
      // IP is set automatically so just copy sellf
      return start_state.copy_self_to_all(this->intra_node);
    }

    auto compare_on = I.getCondition();
    auto applicative_condition = this->value_to_aval(compare_on);

    // if either value is bottom cannot say anything about the comparison
    if (applicative_condition.is_bottom()) {
      return start_state.copy_self_to_all(this->intra_node);
    }

    Expects(I.getNumSuccessors() ==
            2); // conditional should always have a true and false label

    auto true_successor = I.getSuccessor(0);
    auto false_sucessor = I.getSuccessor(1);
    auto true_state = applicative_condition.apply_true(this->start_state);
    auto false_state = applicative_condition.apply_false(this->start_state);

    boost::optional<NodeID> true_nid;
    boost::optional<NodeID> false_nid;
    for (const auto &edge : this->intra_node->getOutEdges()) {
      if (edge->getDstNode()->getBB() == true_successor) {
        true_nid = edge->getDstID();
      }

      if (edge->getDstNode()->getBB() == false_sucessor) {
        false_nid = edge->getDstID();
      }
    }

    Expects(true_nid);
    Expects(false_nid);

    auto res = BlockState(*true_nid, true_state, *false_nid, false_state);
    return res;
  }

  BlockState visitPHINode(PHINode &nd) {
    AbstractValue joiner;
    for (const auto &blk : nd.blocks()) {
      const auto value = nd.getIncomingValueForBlock(blk);
      auto aval = this->value_to_aval(value);
      joiner = joiner.join(aval);
    }

    auto new_state =
        this->start_state.insert_register(this->intra_node->getInst(), joiner);
    ;
    return new_state.copy_self_to_all(this->intra_node);
  }

  BlockState visitStoreInst(StoreInst &store_insn) {

    auto maybe_found = this->intra_node->vPAGEdgeBegin();
    Expects(maybe_found != this->intra_node->vPAGEdgeEnd());
    const PAGEdge *edge_for = *maybe_found;
    const PAGNode *ptr = edge_for->getDstNode();
    Expects(ptr->isPointer());

    auto const to_store = store_insn.getValueOperand();
    auto aval_to_store = this->value_to_aval(to_store);

    auto points_to = this->get_pts(ptr->getId());

    ProgramState mutater = this->start_state;
    for (NodeID nd : points_to) {
      mutater = mutater.insert_memory(nd, aval_to_store);
    }

    return mutater.copy_self_to_all(this->intra_node);
  }

  BlockState visitAlloca(AllocaInst &I) {
    return this->handle_ptr_assign_insn();
  }

  BlockState visitLoadInst(LoadInst &I) {
    auto load_from_nodes = this->get_pts(
        this->intra_node->vPAGEdgeBegin().operator*()->getSrcID());

    AbstractValue start;
    for (auto nd : load_from_nodes) {
      start = start.join(this->start_state.get_alloc(nd));
    }

    auto next =
        this->start_state.insert_register(this->intra_node->getInst(), start);
    return next.copy_self_to_all(this->intra_node);
  }

  BlockState visitReturnInst(ReturnInst &I) {
    // it's safe to not handle intra block rets because rets should get
    // handled interblock
    return this->handle_ptr_assign_insn();
  }
  BlockState visitGetElementPtrInst(GetElementPtrInst &I) {
    return this->handle_ptr_assign_insn();
  }

  BlockState visitFenceInst(FenceInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitAtomicCmpXchgInst(AtomicCmpXchgInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitAtomicRMWInst(AtomicRMWInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitSelectInst(SelectInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitExtractElementInst(ExtractElementInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitInsertElementInst(InsertElementInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitShuffleVectorInst(ShuffleVectorInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitInsertValueInst(InsertValueInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitLandingPadInst(LandingPadInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitTerminator(Instruction &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitUnaryInstruction(llvm::UnaryInstruction &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitFuncletPadInst(llvm::FuncletPadInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitTruncInst(llvm::TruncInst &I) {
    auto op = I.getOperand(0);
    auto to_conv = this->value_to_aval(op);

    auto converted = to_conv.truncate(I.getDestTy());
    auto final_state = this->start_state.insert_register(
        this->intra_node->getInst(), converted);
    return final_state.copy_self_to_all(this->intra_node);
  }

  BlockState visitSExtInst(llvm::SExtInst &I) {
    auto op = I.getOperand(0);
    auto to_conv = this->value_to_aval(op);

    auto converted = to_conv.sext(I.getDestTy());
    auto final_state = this->start_state.insert_register(
        this->intra_node->getInst(), converted);
    return final_state.copy_self_to_all(this->intra_node);
  }

  BlockState visitCmpInst(CmpInst &I) {
    throw std::runtime_error("instruction not handled");
  }

  BlockState visitInstruction(Instruction &I) { return this->visit(I); }

private:
  ProgramState start_state;
  const IntraBlockNode *intra_node;
  const FlowSensitive &flow_results;
};
} // namespace

auto ProgramState::transfer_function(const IntraBlockNode *intra_node,
                                     const FlowSensitive &fpta) -> BlockState {

  TransFunctionVisitor visitor(intra_node, *this, fpta);

  // TODO sorry but visitors in llvm..
  BlockState res =
      visitor.visit(const_cast<Instruction *>(intra_node->getInst()));

  return res;
}

class AddVisitor : boost::static_visitor<ValueEnums> {
public:
  template <typename T>
  auto operator()(const T &i1, const T &i2) const -> ValueEnums {
    return i1.add(i2);
  }

  auto operator()(const ComparisonResult &i1, const ComparisonResult &i2) const
      -> ValueEnums {
    throw std::runtime_error("Can't add comparisons");
  }

  auto operator()(const Bottom &i1, const Bottom &i2) const -> ValueEnums {
    return Bottom();
  }

  template <typename T>
  auto operator()(const Bottom &i1, const T &i2) const -> ValueEnums {
    return Bottom();
  }

  template <typename T>
  auto operator()(const T &i1, const Bottom &i2) const -> ValueEnums {
    return Bottom();
  }

  template <typename T, typename U>
  auto operator()(const T &i1, const U &i2) const -> ValueEnums {
    throw std::runtime_error("Type error");
  }
};

auto AbstractValue::add(const AbstractValue &other) const -> AbstractValue {
  return AbstractValue(
      boost::apply_visitor(AddVisitor(), this->delegate, other.delegate));
}

class EqualityVisitor : boost::static_visitor<bool> {
public:
  template <typename T, typename U>
  auto operator()(const T &i1, const U &i2) const -> bool {
    return false;
  }

  template <typename T>
  auto operator()(const T &i1, const T &i2) const -> bool {
    return i1 == i2;
  }
};

auto AbstractValue::compare(const AbstractValue &other,
                            llvm::CmpInst::Predicate pred,
                            const Value *addr_self,
                            const Value *addr_other) const -> AbstractValue {
  return boost::apply_visitor(ComparisonVisitor(pred, addr_self, addr_other),
                              this->delegate, other.delegate);
}

auto AbstractValue::operator==(const AbstractValue &other) const -> bool {
  return boost::apply_visitor(EqualityVisitor(), this->delegate,
                              other.delegate);
}

using namespace boost;
using namespace numeric;
using namespace interval_lib;

template <typename T, typename U>
auto BaseInt<T, U>::lt(const U &other, const Value *self_addr,
                       const Value *other_addr) const -> AbstractValue {
  ComparisonResult::ConstraintCollection true_collection;
  ComparisonResult::ConstraintCollection false_collection;
  auto this_val = this->get_value();
  auto other_val = other.get_value();

  if (this_val.lower() < other_val.upper()) {
    auto new_this = AbstractValue(U(IntervalAbstract<T>(
        this_val.lower(), std::min(this_val.upper(), other_val.upper() - 1))));
    auto new_other = AbstractValue(U(IntervalAbstract<T>(
        std::max(other_val.lower(), this_val.lower() + 1), other_val.upper())));

    true_collection.insert({other_addr, new_other});
    true_collection.insert({self_addr, new_this});
  }

  if (this_val.upper() >= other_val.lower()) {
    auto new_this = AbstractValue(U(IntervalAbstract<T>(
        std::max(this_val.lower(), other_val.lower()), this_val.upper())));
    auto new_other = AbstractValue(U(IntervalAbstract<T>(
        other_val.lower(), std::min(this_val.upper(), other_val.upper()))));
    false_collection.insert({other_addr, new_other});
    false_collection.insert({self_addr, new_this});
  }

  return AbstractValue(ComparisonResult(true_collection, false_collection));
}

// TODO essential we need a way to handle comparison better, ==  needs to check
// that structure is same also represent infinity
template <typename T, typename U>
auto BaseInt<T, U>::eq(const U &other, const Value *self_addr,
                       const Value *other_addr) const -> AbstractValue {
  ComparisonResult::ConstraintCollection true_collection;
  ComparisonResult::ConstraintCollection false_collection;

  try {
    auto res = intersect(this->value, other.value);
    auto intersected = AbstractValue(U(res));
    if (!empty(res)) {
      true_collection.insert({self_addr, intersected});
      true_collection.insert({other_addr, intersected});
    }
  } catch (std::runtime_error e) {
  }

  auto onot = other.value;
  auto tnot = this->value;

  if (!singleton(onot) || !singleton(tnot) || tnot != onot) {
    false_collection.insert({other_addr, AbstractValue(U(onot))});

    false_collection.insert({self_addr, AbstractValue(U(tnot))});
  }

  return AbstractValue(ComparisonResult(true_collection, false_collection));
}

template <typename T, typename U>
auto BaseInt<T, U>::to_string() const -> std::string {
  return "[" + std::to_string(this->value.lower()) + "," +
         std::to_string(this->value.upper()) + "]";
}

template <typename T, typename U>
auto BaseInt<T, U>::add(const U &other) const -> U {
  return {other.get_value() + this->value};
}

template <typename T, typename U>
auto BaseInt<T, U>::join(const U &other) const -> U {
  auto new_int = hull(this->value, other.value);

  return new_int;
}

template <typename T, typename U>
auto BaseInt<T, U>::get_value() const -> const IntervalAbstract<T> & {
  return this->value;
}

template <typename T, typename U>
auto BaseInt<T, U>::operator==(const U &other) const -> bool {
  if (empty(this->value)) {
    return empty(other.value);
  }
  return this->value.lower() == other.value.lower() &&
         this->value.upper() == other.value.upper();
}

auto IntI64::trunc_i32() const -> AbstractValue {
  auto val = this->get_value();
  auto new_lower = static_cast<int32_t>(val.lower());
  auto new_upper = static_cast<int32_t>(val.upper());
  return AbstractValue(IntI32(IntervalAbstract<int32_t>(new_lower, new_upper)));
}

auto IntI32::sext_i64() const -> AbstractValue {
  auto val = this->get_value();
  auto new_lower = static_cast<int64_t>(val.lower());
  auto new_upper = static_cast<int64_t>(val.upper());
  return AbstractValue(IntI64(IntervalAbstract<int64_t>(new_lower, new_upper)));
}

class JoinVisitor : boost::static_visitor<ValueEnums> {
public:
  // TODO I think there might be a way to handle this but not sure if this case
  // can ever occur
  auto operator()(const ComparisonResult &i1, const ComparisonResult &i2) const
      -> ValueEnums {

    return i1.join(i2);
  }

  template <typename T>
  auto operator()(const T &i1, const T &i2) const -> ValueEnums {
    return i1.join(i2);
  }

  template <typename T, typename U>
  auto operator()(const T &i1, const U &i2) const -> ValueEnums {
    throw std::runtime_error("Type error");
  }

  auto operator()(const Bottom &i1, const Bottom &i2) const -> ValueEnums {
    return Bottom();
  }

  template <typename T>
  auto operator()(const Bottom &i1, const T &i2) const -> ValueEnums {
    return i2;
  }

  template <typename T>
  auto operator()(const T &i1, const Bottom &i2) const -> ValueEnums {
    return i1;
  }
};

auto AbstractValue::join(const AbstractValue &mt) const -> AbstractValue {
  return AbstractValue(
      boost::apply_visitor(JoinVisitor(), this->delegate, mt.delegate));
}

AbstractValue::AbstractValue(ValueEnums val) : delegate(std::move(val)) {}

auto AbstractValue::create_u64(const IntervalAbstract<uint64_t> &val)
    -> AbstractValue {
  return {IntU64(val)};
}

auto ProgramState::insert_memory(NodeID key, AbstractValue val) const
    -> ProgramState {
  return ProgramState(this->registers,
                      std::move(this->alocs.insert(key, std::move(val))),
                      this->loop_counter);
}

AbstractValue::AbstractValue(const llvm::Constant *cst) {
  if (const auto cst_int = llvm::dyn_cast<llvm::ConstantInt>(cst)) {
    if (cst_int->getType()->getSignBit() != 0) {
      switch (cst_int->getBitWidth()) {
      case 64:
        this->delegate = IntI64(cst_int->getSExtValue());
        return;
      case 32:
        this->delegate = IntI32(cst_int->getSExtValue());
        return;
      default:
        throw std::runtime_error("unimplemented cosntant type");
      }
    }
    switch (cst_int->getBitWidth()) {
    case 64:
      this->delegate = IntU64(cst_int->getZExtValue());
      return;
    case 32:
      this->delegate = IntU32(cst_int->getZExtValue());
      return;
    default:
      throw std::runtime_error("unimplemented cosntant type");
    }
  } else {
    throw std::runtime_error("unimplemented cosntant type");
  }
}

auto ProgramState::insert_register(const Value *key, AbstractValue val) const
    -> ProgramState {
  return ProgramState(std::move(this->registers.insert(key, val)), this->alocs,
                      this->loop_counter);
}

template <typename K>
auto ValueHolder<K>::insert(K key, AbstractValue val) const -> ValueHolder {
  std::unordered_map<K, AbstractValue> map_copy = this->values;
  map_copy[key] = val;
  return ValueHolder<K>(std::move(map_copy));
}

class AsU64 : boost::static_visitor<boost::optional<IntU64>> {
public:
  auto operator()(const IntU64 &i1) const -> boost::optional<IntU64> {
    boost::optional<IntU64> f;
    f = i1;
    return f;
  }

  template <typename T>
  auto operator()(const T &i1) const -> boost::optional<IntU64> {
    boost::optional<IntU64> f;
    return f;
  }
};

auto AbstractValue::truncate(const llvm::Type *ty) const -> AbstractValue {
  return boost::apply_visitor(TruncVisitor(ty), this->delegate);
}

namespace {

class SextVisitor : boost::static_visitor<AbstractValue> {
public:
  SextVisitor(const llvm::Type *ty) : to_type(ty) {}
  auto operator()(const Bottom &i1) const -> AbstractValue {
    return AbstractValue(Bottom());
  }

  auto operator()(const ComparisonResult &i1) const -> AbstractValue {
    throw std::runtime_error("trying to truncate a bool");
  }

  auto operator()(const Convertable &i1) const -> AbstractValue {
    if (const auto int_type =
            llvm::dyn_cast<llvm::IntegerType>(this->to_type)) {
      if (int_type->getSignBit() != 0) {
        switch (int_type->getBitWidth()) {
        case 64:
          return i1.sext_i64();
        case 32:
          return i1.sext_i32();
        default:
          throw std::runtime_error("Trunc type error");
        }
      } else {
        switch (int_type->getBitWidth()) {
        case 64:
          return i1.sext_u64();
        case 32:
          return i1.sext_u32();
        default:
          throw std::runtime_error("Trunc type error");
        }
      }
    } else {
      throw std::runtime_error("Trunc type error");
    }
  }

private:
  const llvm::Type *to_type;
};

} // namespace

auto AbstractValue::sext(const llvm::Type *ty) const -> AbstractValue {
  return boost::apply_visitor(SextVisitor(ty), this->delegate);
}

auto AbstractValue::as_u64() const -> boost::optional<IntU64> {
  return boost::apply_visitor(AsU64(), this->delegate);
}

auto ProgramState::zero_counter() const -> ProgramState {
  ProgramState copy = *this;
  copy.loop_counter = AbstractValue::create_u64(0);
  return copy;
}

auto ProgramState::increment_counter() const -> ProgramState {
  ProgramState copy = *this;
  auto loop_count = this->loop_counter.as_u64();
  if (loop_count) {
    auto new_count = loop_count->get_value() + IntervalAbstract<uint64_t>(1);
    copy.loop_counter = AbstractValue::create_u64(new_count);
  } else {
    copy.loop_counter = AbstractValue::create_u64(1);
  }

  return copy;
}

template <typename K>
auto ValueHolder<K>::widen(const ValueHolder<K> &other) const
    -> ValueHolder<K> {
  std::unordered_map<K, AbstractValue> values = this->values;
  std::unordered_map<K, AbstractValue> widen_upper = this->widen_upper;
  std::unordered_map<K, AbstractValue> widen_lower = this->widen_lower;
  return ValueHolder<K>(values, widen_upper, widen_lower);
}

auto ProgramState::widen(const ProgramState &other) const -> ProgramState {
  auto alocs_widened = this->alocs.widen(other.alocs);
  auto register_widened = this->registers.widen(other.registers);
  return ProgramState(register_widened, alocs_widened, this->loop_counter);
}

auto BlockState::widen(const BlockState &other) const -> BlockState {
  std::unordered_map<NodeID, ProgramState> succ_to_state_next;
  for (const auto &other : other.succ_to_state) {
    Expects(this->succ_to_state.find(other.first) != this->succ_to_state.end());
    succ_to_state_next[other.first] =
        this->succ_to_state.find(other.first)->second.widen(other.second);
  }

  return BlockState(std::move(succ_to_state_next));
}

template <typename K>
auto ValueHolder<K>::get_value(K key) const -> AbstractValue {
  auto fnd = this->values.find(key);
  if (fnd != this->values.end()) {
    return fnd->second;
  } else {
    return AbstractValue(Bottom());
  }
}

template <typename K>
auto ValueHolder<K>::join(const ValueHolder<K> &other) const -> ValueHolder<K> {
  auto next_map = this->values;
  for (const auto &kv : other.values) {
    AbstractValue curr = this->get_value(kv.first);
    next_map[kv.first] = curr.join(kv.second);
  }

  return ValueHolder(next_map);
}

auto ProgramState::to_string() const -> std::string {
  std::string value;
  for (auto const &reg : this->registers.values) {
    value += reg.first->getName().str() + ": " + reg.second.to_string() + "\n";
  }

  return value;
}

auto BlockState::to_string() const -> std::string {
  std::string value;
  for (const auto &st : this->succ_to_state) {
    value += "Block State Begin\n" + std::to_string(st.first) + ":" + "\n" +
             st.second.to_string() + "\n-----Block State End";
  }
  return value;
}

auto IntervalAnalysis::get_state(NodeID for_blk) -> BlockState {
  auto fnd = this->values.find(for_blk);
  if (fnd == this->values.end()) {
    return BlockState();
  } else {
    return fnd->second;
  }
}

namespace {
class ComparisonApplicationVisitor : static_visitor<ProgramState> {
private:
  const ProgramState &prev_state;
  bool is_true;

public:
  ComparisonApplicationVisitor(const ProgramState &prev_state, bool is_true)
      : prev_state(prev_state), is_true(is_true) {}

  auto operator()(const ComparisonResult &i1) const -> ProgramState {
    if (this->is_true) {
      return i1.apply_true(this->prev_state);
    } else {
      return i1.apply_false(this->prev_state);
    }
  };

  template <typename T> auto operator()(const T &i1) const -> ProgramState {
    throw std::runtime_error("Applying not a comaprison");
  }
};
} // namespace

auto AbstractValue::apply_true(const ProgramState &st) const -> ProgramState {
  return boost::apply_visitor(ComparisonApplicationVisitor(st, true),
                              this->delegate);
}

auto AbstractValue::apply_false(const ProgramState &st) const -> ProgramState {
  return boost::apply_visitor(ComparisonApplicationVisitor(st, false),
                              this->delegate);
}

namespace {
auto apply_comparison(const ProgramState &st,
                      const ComparisonResult::ConstraintCollection &reses) {
  auto mutate = st;
  for (const auto &constraint : reses) {
    auto key = constraint.first;
    auto value = constraint.second;
    mutate = mutate.insert_register(key, value);
  }
  return mutate;
}
} // namespace

auto ComparisonResult::apply_false(const ProgramState &st) const
    -> ProgramState {
  return apply_comparison(st, this->false_constraints);
}

auto ComparisonResult::apply_true(const ProgramState &st) const
    -> ProgramState {
  return apply_comparison(st, this->true_constraints);
}

namespace {
class IsBottom : static_visitor<ProgramState> {
public:
  auto operator()(const Bottom &i1) const -> bool { return true; }
  template <typename T> auto operator()(const T &i1) const -> bool {
    return false;
  }
};
} // namespace

auto AbstractValue::is_bottom() const -> bool {
  return boost::apply_visitor(IsBottom(), this->delegate);
}

namespace {
class ToStringVisitor : static_visitor<std::string> {
public:
  template <typename T> auto operator()(const T &val) -> std::string {
    return val.to_string();
  }
};
} // namespace

auto AbstractValue::to_string() const -> std::string {
  return boost::apply_visitor(ToStringVisitor(), this->delegate);
}

auto ProgramState::val_to_aval(const Value *val) const -> AbstractValue {
  if (const auto cst = llvm::dyn_cast<llvm::Constant>(val)) {
    return AbstractValue(cst);
  } else {
    return this->get_register(val);
  }
}

auto ComparisonResult::join(const ComparisonResult &other) const
    -> ComparisonResult {
  std::unordered_map<const Value *, AbstractValue> new_trues =
      this->true_constraints;

  std::unordered_map<const Value *, AbstractValue> new_false =
      this->false_constraints;
  for (const auto &pair : other.true_constraints) {
    if (new_trues.find(pair.first) != new_trues.end()) {
      new_trues[pair.first] =
          pair.second.join(new_trues.find(pair.first)->second);
    } else {
      new_trues[pair.first] = pair.second;
    }
  }

  for (const auto &pair : other.false_constraints) {
    if (new_false.find(pair.first) != new_false.end()) {
      new_false[pair.first] =
          pair.second.join(new_false.find(pair.first)->second);
    } else {
      new_false[pair.first] = pair.second;
    }
  }

  return ComparisonResult(std::move(new_trues), std::move(new_false));
}