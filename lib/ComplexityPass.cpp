#include <ComplexityPass.hpp>
#include <Graphs/ICFG.h>
#include <Graphs/PAG.h>
#include <IntervalAnalysis.hpp>
#include <boost/optional.hpp>
#include <functional>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <utility>

using namespace boost::numeric;
using namespace boost;
using namespace std;

ComplexityPass::ComplexityPass(const FlowSensitive &mod,
                               gsl::not_null<const Instruction *> sink)
    : ComplexityPass(mod, SVFUtil::getProgEntryFunction(mod.getModule()),
                     sink) {}
ComplexityPass::ComplexityPass(const FlowSensitive &mod,
                               gsl::not_null<const Function *> entry_func,
                               gsl::not_null<const Instruction *> sink)
    : fpa(mod), entry_func(entry_func), sink(sink), loop_info(LoopHandler()) {}

auto ComplexityPass::calculate_complexity(const Instruction *from) const
    -> boost::optional<IntervalAbstract<uint64_t>> {
  const auto icfg = this->fpa.getPAG()->getICFG();
  auto source_id = icfg->getBlockICFGNode(from)->getId();
  auto sink_id = icfg->getBlockICFGNode(this->sink)->getId();
  std::unordered_set<NodeID> target{sink_id};
  return this->calculate_complexity(source_id, target);
}

auto ComplexityPass::forward_slice(NodeID from) const
    -> std::unordered_set<NodeID> {
  std::unordered_set<NodeID> result;
  ICFG *icfg = this->fpa.getPAG()->getICFG();
  queue<const ICFGNode *> worklist;
  worklist.push(icfg->getICFGNode(from));
  while (!worklist.empty()) {
    const ICFGNode *curr_node = worklist.front();
    worklist.pop();

    for (const ICFGEdge *curr_edge : curr_node->getOutEdges()) {
      ICFGNode *dst_node = curr_edge->getDstNode();
      if (result.find(dst_node->getId()) == result.end()) {
        worklist.push(dst_node);
      }
    }

    result.insert(curr_node->getId());
  }
  return result;
}

auto ComplexityPass::reverse_slice(const std::unordered_set<NodeID> &targets)
    const -> std::unordered_set<NodeID> {
  std::unordered_set<NodeID> result;
  ICFG *icfg = this->fpa.getPAG()->getICFG();
  for (NodeID target : targets) {
    queue<const ICFGNode *> worklist;
    worklist.push(icfg->getICFGNode(target));
    while (!worklist.empty()) {
      const ICFGNode *curr_node = worklist.front();
      worklist.pop();

      for (const ICFGEdge *curr_edge : curr_node->getInEdges()) {
        ICFGNode *dst_node = curr_edge->getSrcNode();
        if (result.find(dst_node->getId()) == result.end()) {
          worklist.push(dst_node);
        }
      }

      result.insert(curr_node->getId());
    }
  }
  return result;
}

/*    ;*/

auto ComplexityPass::calculate_block_cost(
    const BasicBlock *curr_block, const Instruction *curr_insn, ICFG *icfg,
    boost::optional<IntervalAbstract<uint64_t>> union_prev,
    NodeID curr_node_id) const -> boost::optional<IntervalAbstract<uint64_t>> {
  const Instruction *first_insn = &*curr_block->begin();
  IntervalAbstract<uint64_t> cost = 1;
  if (first_insn == curr_insn && this->loop_info.is_loop_head(curr_block)) {
    cost = this->loop_info.get_loop_cost(curr_node_id, *this, icfg);
  }
  return union_prev ? *union_prev + cost : cost;
}

auto ComplexityPass::get_interesting_nodes(
    NodeID from, const std::unordered_set<NodeID> &targets) const
    -> std::unordered_set<NodeID> {
  std::unordered_set<NodeID> result;
  auto forward = this->forward_slice(from);
  auto reverse = this->reverse_slice(targets);

  Expects(!forward.empty());
  Expects(!reverse.empty());
  for (const NodeID nid : forward) {
    if (reverse.find(nid) != reverse.end()) {
      result.insert(nid);
    }
  }
  Expects(!result.empty());
  return result;
}

auto ComplexityPass::calculate_complexity(
    NodeID from, const unordered_set<NodeID> &targets) const
    -> boost::optional<IntervalAbstract<uint64_t>> {

  ICFG *icfg = this->fpa.getPAG()->getICFG();
  unordered_map<NodeID, IntervalAbstract<uint64_t>> values;
  /*queue<std::pair<const BasicBlock *, unordered_set<const BasicBlock *>>>
   worklist; // pair of the next block
   // and how we got there*/
  queue<std::pair<NodeID, unordered_set<NodeID>>> worklist;

  /*worklist.push(std::make_pair(from, unordered_set<const BasicBlock *>())); //
   this traversal could probably be
   // made more effecient by doing a
   // toplogical sort... would be
   // annoying tho, possibly we need a
   // class for representing subgraphs
   // of the ICFG. Topological sort
   // would also detect loops so we
   // wouldnt need to carry around
   // path info*/

  worklist.push(std::make_pair(from, unordered_set<NodeID>()));

  auto interesting_nodes = this->get_interesting_nodes(from, targets);
  while (!worklist.empty()) {
    auto current = worklist.front();
    worklist.pop();

    auto current_node_id = current.first;
    auto current_path = current.second;

    if (current_path.find(current_node_id) != current_path.end()) {
      throw std::runtime_error(
          "Mutually recursive loop between start and target");
    }

    std::cout << "executing search" << std::endl;
    current_path.insert(current_node_id);

    boost::optional<IntervalAbstract<uint64_t>> previous_value_current_node;

    if (values.find(current_node_id) != values.end()) {
      previous_value_current_node = values[current_node_id];
    }

    const ICFGNode *curr_icfg_node = icfg->getICFGNode(current_node_id);

    boost::optional<IntervalAbstract<uint64_t>> union_prev;
    for (const ICFGEdge *curr_edge : curr_icfg_node->getInEdges()) {
      if (values.find(curr_edge->getSrcID()) != values.end()) {
        if (union_prev) {
          union_prev =
              hull(*union_prev, values.find(curr_edge->getSrcID())->second);
        } else {
          union_prev = values.find(curr_edge->getSrcID())->second;
        }
      }
    }

    boost::optional<IntervalAbstract<uint64_t>> new_value;

    if (const auto *intra_block =
            llvm::dyn_cast<IntraBlockNode>(curr_icfg_node)) {
      auto cur_bbnode = intra_block->getBB();
      auto curr_inst = intra_block->getInst();
      new_value = this->calculate_block_cost(cur_bbnode, curr_inst, icfg,
                                             union_prev, current_node_id);
    } else if (const auto *call_block =
                   llvm::dyn_cast<CallBlockNode>(curr_icfg_node)) {
      new_value = this->calculate_block_cost(
          call_block->getCallSite().getInstruction()->getParent(),
          call_block->getCallSite().getInstruction(), icfg, union_prev,
          current_node_id);
    } else {
      new_value = union_prev;
    }

    if (new_value) {
      std::cout << "executed update" << std::endl;
      values[current_node_id] = *new_value;
    }

    // if not target and we changed our answer push successors: so without
    // topological sort we may end up updating a node too many times could add
    // logical clock to skip
    if (targets.find(current_node_id) == targets.end() &&
        new_value != previous_value_current_node) {

      auto curr_bbnode = curr_icfg_node->getBB();
      if (this->loop_info.is_loop_head(curr_bbnode)) {
        for (auto item : this->loop_info.get_loop_exits(curr_bbnode)) {
          const NodeID next_node_id =
              icfg->getBlockICFGNode(&*item->begin())->getId();
          if (interesting_nodes.find(next_node_id) != interesting_nodes.end()) {
            worklist.push(std::make_pair(next_node_id, current_path));
          }
        }
      } else {
        for (const ICFGEdge *out_edge : curr_icfg_node->getOutEdges()) {
          const NodeID next_node_id = out_edge->getDstNode()->getId();
          if (interesting_nodes.find(next_node_id) != interesting_nodes.end()) {
            worklist.push(std::make_pair(next_node_id, current_path));
          }
        }
      }
    }
  }

  boost::optional<IntervalAbstract<uint64_t>> total;
  for (NodeID target : targets) {
    if (values.find(target) != values.end()) {
      if (total) {
        total = hull(*total, values[target]);
      } else {
        total = values[target];
      }
    }
  }

  return total;
}
