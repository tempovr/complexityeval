#ifndef B9D8EA50_4F64_4262_B6B7_856F97722666
#define B9D8EA50_4F64_4262_B6B7_856F97722666
#include <IntervalAnalysis.hpp>
#include <SVF-FE/DataFlowUtil.h>
#include <WPA/FlowSensitive.h>
#include <boost/numeric/interval.hpp>
#include <unordered_map>
#include <unordered_set>

const unsigned int SMALL_VEC_SIZE = 10;

using BlockCollection = llvm::SmallVector<BasicBlock *, SMALL_VEC_SIZE>;
class ComplexityPass;
class LoopHandler {
private:
  mutable PTACFInfoBuilder delegate;
  mutable std::unordered_map<const BasicBlock *,
                             boost::numeric::interval<uint64_t>>
      cached_counts;

  auto calculate_loop_count(const BasicBlock *header, const FlowSensitive &fpta,
                            const Function *entry_point) const
      -> IntervalAbstract<uint64_t>;

public:
  LoopHandler() : delegate(PTACFInfoBuilder()) {}
  auto is_loop_head(const BasicBlock *block) const -> bool;
  auto get_loop_cost(NodeID block, const ComplexityPass &passmanager,
                     ICFG *icfg) const -> IntervalAbstract<uint64_t>;
  auto get_loop_exits(const BasicBlock *block) const
      -> llvm::SmallVector<BasicBlock *, SMALL_VEC_SIZE>;
};
#endif /* B9D8EA50_4F64_4262_B6B7_856F97722666 */
