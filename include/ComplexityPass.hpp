#ifndef EAFC2AD6_3006_4ABD_B275_F1E7A3627AA6
#define EAFC2AD6_3006_4ABD_B275_F1E7A3627AA6
#include <Graphs/SVFG.h>
#include <IntervalAnalysis.hpp>
#include <LoopHandler.hpp>
#include <SVF-FE/LLVMUtil.h>
#include <WPA/FlowSensitive.h>
#include <boost/numeric/interval.hpp>
#include <gsl/pointers>
#include <unordered_set>

// we need a module pass because llvm doesnt have an interprocedurla CFG

class ComplexityPass {
private:
  const FlowSensitive &fpa;
  gsl::not_null<const Function *> entry_func;
  gsl::not_null<const Instruction *> sink; // attacker sinks
  LoopHandler loop_info;

public:
  ComplexityPass(const FlowSensitive &mod,
                 gsl::not_null<const Instruction *> sink); // TODO NodeID
  ComplexityPass(const FlowSensitive &mod,
                 gsl::not_null<const Function *> entry_func,
                 gsl::not_null<const Instruction *> sink);

  auto calculate_complexity(const Instruction *from) const
      -> boost::optional<IntervalAbstract<uint64_t>>; // these intervals should
                                                      // probably be unchecked
                                                      // closed

private:
  friend class LoopHandler;
  auto forward_slice(NodeID from) const -> std::unordered_set<NodeID>;
  auto reverse_slice(const std::unordered_set<NodeID> &targets) const
      -> std::unordered_set<NodeID>;
  auto get_interesting_nodes(NodeID from,
                             const std::unordered_set<NodeID> &targets) const
      -> std::unordered_set<NodeID>;
  auto calculate_complexity(NodeID from,
                            const std::unordered_set<NodeID> &targets) const
      -> boost::optional<IntervalAbstract<uint64_t>>;

  auto calculate_block_cost(const BasicBlock *curr_block,
                            const Instruction *curr_insn, ICFG *icfg,
                            boost::optional<IntervalAbstract<uint64_t>> union_prev,
                            NodeID curr_node_id) const
      -> boost::optional<IntervalAbstract<uint64_t>>;
};
#endif /* EAFC2AD6_3006_4ABD_B275_F1E7A3627AA6 */
