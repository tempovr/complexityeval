#ifndef D87C2643_53E9_4BB1_836F_78334D9F5BB4

#include <MemoryModel/MemModel.h>
#include <MemoryModel/PointerAnalysis.h>
#include <SVF-FE/LLVMUtil.h>
#include <WPA/FlowSensitive.h>
#include <boost/numeric/interval.hpp>
#include <boost/numeric/interval/checking.hpp>
#include <boost/numeric/interval/policies.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>
#include <functional>
#include <gsl/pointers>
#include <llvm/Support/Casting.h>
#include <unordered_map>
#include <unordered_set>
template <typename G> using IntervalAbstract = boost::numeric::interval<G>;
// using IntervalAbstract =
//  boost::numeric::interval_lib::change_checking<
//    boost::numeric::interval<G>,
//  boost::numeric::interval_lib::checking_base<G>>; //
/**
 * Lots of dynamic dispatch for these visitors... not sure if we can use CRTP
 * given that these get dumped into a single map
 */

class Bottom {
public:
  auto operator==(const Bottom &other) const -> bool { return true; }
  auto to_string() const -> std::string { return "Bottom"; }
};

class IntU64;
class IntI64;
class IntI32;
class IntU32;
class ComparisonResult;

using ValueEnums = boost::variant<
    boost::recursive_wrapper<ComparisonResult>, Bottom,
    boost::recursive_wrapper<IntU64>, boost::recursive_wrapper<IntU32>,
    boost::recursive_wrapper<IntI32>, boost::recursive_wrapper<IntI64>>;

class ProgramState;
// using ValueEnums = boost::variant<Bottom, IntU64, IntU32, IntI32, IntI64>;
class AbstractValue {
public:
  static auto create_u64(const IntervalAbstract<uint64_t> &val)
      -> AbstractValue;

  auto to_string() const -> std::string;
  auto join(const AbstractValue &other) const -> AbstractValue;
  auto operator==(const AbstractValue &rhs) const -> bool;
  auto operator!=(const AbstractValue &rhs) const -> bool;
  auto is_bottom() const -> bool;
  auto apply_true(const ProgramState &st) const -> ProgramState;
  auto apply_false(const ProgramState &st) const -> ProgramState;
  auto truncate(const llvm::Type *to_ty) const -> AbstractValue;
  auto sext(const llvm::Type *to_ty) const -> AbstractValue;
  auto add(const AbstractValue &other) const -> AbstractValue;
  auto compare(const AbstractValue &other, llvm::CmpInst::Predicate pred,
               const Value *addr_self, const Value *addr_other) const
      -> AbstractValue;
  auto as_u64() const -> boost::optional<IntU64>;

  AbstractValue(ValueEnums);
  AbstractValue();
  AbstractValue(const llvm::Constant *);

private:
  ValueEnums delegate;
};

template <typename K> class ValueHolder {
private:
  std::unordered_map<K, AbstractValue> values;
  std::unordered_map<K, AbstractValue> widen_upper;
  std::unordered_map<K, AbstractValue> widen_lower;
  friend class ProgramState;

public:
  ValueHolder(std::unordered_map<K, AbstractValue> values,
              std::unordered_map<K, AbstractValue> widen_upper,
              std::unordered_map<K, AbstractValue> widen_lower)
      : values(values), widen_upper(widen_upper), widen_lower(widen_lower) {}
  ValueHolder() = default;
  ValueHolder(std::unordered_map<K, AbstractValue> values)
      : values(std::move(values)) {}

  auto get_value(K key) const -> AbstractValue;
  auto insert(K key, AbstractValue val) const -> ValueHolder;
  auto operator==(const ValueHolder<K> &other) const -> bool {
    return other.values == this->values;
  }
  auto widen(const ValueHolder<K> &other) const -> ValueHolder<K>;
  auto join(const ValueHolder<K> &other) const -> ValueHolder<K>;
};

/*
class ConstraintHash;
struct ComparisonConstraint {
  friend class ConstraintHash;

public:
  const Value *key;
  AbstractValue value;

  ComparisonConstraint(const Value *key, AbstractValue value)
      : key(key), value(std::move(value)) {}

  auto operator==(const ComparisonConstraint &other) const -> bool {
    return other.key == this->key && other.value == this->value;
  }
};

class ConstraintHash {
public:
  size_t operator()(const ComparisonConstraint &comp) const {
    return std::hash<const Value *>()(comp.key);
    // TODO dont do this^^^
  }
};*/

// TODO Results of comaprisons should enforce their constraints on values up the
// def use chain
class ComparisonResult {

public:
  using ConstraintCollection = std::unordered_map<const Value *, AbstractValue>;

  ComparisonResult() = default;
  ComparisonResult(ConstraintCollection true_cons,
                   ConstraintCollection false_cons)
      : true_constraints(std::move(true_cons)),
        false_constraints(std::move(false_cons)) {}
  auto apply_true(const ProgramState &st) const -> ProgramState;
  auto apply_false(const ProgramState &st) const -> ProgramState;
  auto to_string() const -> std::string {
    std::string tot = "Start-----";

    for (auto const &val : this->true_constraints) {
      tot += val.first->getName().str() + " " + val.second.to_string() + "\n";
    }
    tot += "----End";
    return tot;
  }

  auto join(const ComparisonResult &other) const -> ComparisonResult;
  auto operator==(const ComparisonResult &other) const -> bool {
    return other.true_constraints == this->true_constraints &&
           this->false_constraints == other.false_constraints;
  }

private:
  ConstraintCollection true_constraints;
  ConstraintCollection false_constraints;
};

class Convertable {
public:
  virtual auto trunc_i64() const -> AbstractValue = 0;

  virtual auto trunc_u64() const -> AbstractValue = 0;

  virtual auto trunc_u32() const -> AbstractValue = 0;

  virtual auto trunc_i32() const -> AbstractValue = 0;

  virtual auto sext_i64() const -> AbstractValue = 0;

  virtual auto sext_u64() const -> AbstractValue = 0;

  virtual auto sext_u32() const -> AbstractValue = 0;

  virtual auto sext_i32() const -> AbstractValue = 0;
};
template <typename T, typename U> class BaseInt : public Convertable {
public:
protected:
  BaseInt(const IntervalAbstract<T> &val) : value(val) {}

public:
  auto add(const U &other) const -> U;
  auto join(const U &other) const -> U;
  auto to_string() const -> std::string;
  auto get_value() const -> const IntervalAbstract<T> &;
  auto operator==(const U &other) const -> bool;
  auto eq(const U &other, const Value *self_addr, const Value *other_addr) const
      -> AbstractValue;
  auto lt(const U &other, const Value *self_addr, const Value *other_addr) const
      -> AbstractValue;

  virtual auto trunc_i64() const -> AbstractValue {
    throw std::runtime_error("invalid trunc i64");
  }

  virtual auto trunc_u64() const -> AbstractValue {
    throw std::runtime_error("invalid trunc u64");
  }

  virtual auto trunc_u32() const -> AbstractValue {
    throw std::runtime_error("invalid trunc u32");
  }

  virtual auto trunc_i32() const -> AbstractValue {
    throw std::runtime_error("invalid trunc i32");
  }

  virtual auto sext_i64() const -> AbstractValue {
    throw std::runtime_error("invalid sext i64");
  }

  virtual auto sext_u64() const -> AbstractValue {
    throw std::runtime_error("invalid sext u64");
  }

  virtual auto sext_u32() const -> AbstractValue {
    throw std::runtime_error("invalid sext u32");
  }

  virtual auto sext_i32() const -> AbstractValue {
    throw std::runtime_error("invalid sext i32");
  }

private:
  IntervalAbstract<T> value;
};
class IntU64 : public BaseInt<uint64_t, IntU64> {
public:
  IntU64(const IntervalAbstract<uint64_t> &val) : BaseInt(val) {}
};

class IntU32 : public BaseInt<uint32_t, IntU32> {
public:
  IntU32(const IntervalAbstract<uint32_t> &val) : BaseInt(val) {}
};

class IntI32 : public BaseInt<int32_t, IntI32> {
public:
  IntI32(const IntervalAbstract<int32_t> &val) : BaseInt(val) {}
  virtual auto sext_i64() const -> AbstractValue;
};

class IntI64 : public BaseInt<int64_t, IntI64> {
public:
  virtual auto trunc_i32() const -> AbstractValue;
  IntI64(const IntervalAbstract<int64_t> &val) : BaseInt(val) {}
};

class BlockState;
struct ProgramState {
private:
  AbstractValue loop_counter;
  // SSA so registers are instructions values are pointers
  ValueHolder<const Value *> registers;

  // memory locations
  ValueHolder<NodeID> alocs; // these are the NodeIDs of the memory obejcts,
                             // this is slightly wrong because we wont handle
                             // overlapping aggregate types

  ProgramState(ValueHolder<const Value *> registers, ValueHolder<SymID> alocs,
               AbstractValue loop_counter)
      : registers(std::move(registers)), alocs(std::move(alocs)),
        loop_counter(std::move(loop_counter)) {}

public:
  auto widen(const ProgramState &other) const -> ProgramState;
  auto val_to_aval(const Value *val) const -> AbstractValue;
  ProgramState() = default;
  auto to_string() const -> std::string;
  auto increment_counter() const -> ProgramState;
  auto zero_counter() const -> ProgramState;
  auto copy_self_to_all(const ICFGNode *blk) const -> BlockState;
  auto insert_register(const Value *key,
                       AbstractValue value) const
      -> ProgramState; // TODO this shou;d be debug only
  auto insert_memory(NodeID key,
                     AbstractValue value) const
      -> ProgramState; // TODO this should be debug only
  auto clone() const -> ProgramState;
  auto operator==(const ProgramState &rhs) const -> bool;
  auto operator!=(const ProgramState &rhs) const -> bool;
  auto transfer_function(const IntraBlockNode *, const FlowSensitive &fpta)
      -> BlockState;
  auto apply_entry_block(const FunEntryBlockNode *) const -> BlockState;
  auto apply_ret_block(const RetBlockNode *) const -> BlockState;
  auto join(const ProgramState &other) const -> ProgramState;
  inline auto get_loop_counter() const -> AbstractValue {
    return this->loop_counter;
  }

  inline auto get_register(const Value *key) const -> AbstractValue {
    return this->registers.get_value(key);
  }

  inline auto get_alloc(NodeID key) -> AbstractValue {
    return this->alocs.get_value(key);
  }

  inline auto get_register_as_u64(const Value *key) const
      -> boost::optional<IntervalAbstract<uint64_t>> {
    boost::optional<IntervalAbstract<uint64_t>> nval;
    auto aval = this->registers.get_value(key);
    if (aval.as_u64()) {
      nval = aval.as_u64()->get_value();
    }

    return nval;
  }
};

struct BlockState {
private:
  std::unordered_map<NodeID, ProgramState> succ_to_state;

public:
  auto widen(const BlockState &other) const -> BlockState;
  BlockState(NodeID true_blk, ProgramState true_state, NodeID flase_blk,
             ProgramState false_state) {
    this->succ_to_state.insert({true_blk, true_state});
    this->succ_to_state.insert({flase_blk, false_state});
  }
  inline auto get_state(NodeID succ) const -> ProgramState {
    if (succ_to_state.find(succ) != succ_to_state.end()) {
      return succ_to_state.find(succ)->second;
    }
    return ProgramState();
  }

  auto to_string() const -> std::string;

  inline auto get_counter() const -> AbstractValue {

    if (this->succ_to_state.begin() == this->succ_to_state.end()) {
      return AbstractValue(Bottom());
    }

    return this->succ_to_state.begin()->second.get_loop_counter();
  }

  BlockState() = default;
  BlockState(std::unordered_map<NodeID, ProgramState> succ_to_state)
      : succ_to_state(std::move(succ_to_state)) {}

  auto operator==(const BlockState &rhs) const -> bool;
  auto operator!=(const BlockState &rhs) const -> bool;
};

class IntervalAnalysis {
public:
  IntervalAnalysis(const FlowSensitive &fpta,
                   gsl::not_null<const BasicBlock *> counter,
                   gsl::not_null<const BasicBlock *> zeroer,
                   gsl::not_null<const Function *> entry_point);
  auto get_loop_count() const -> AbstractValue;

  auto get_state(NodeID for_blk) -> BlockState;

private:
  const FlowSensitive &fpta;
  NodeID counter_block;
  NodeID zeroer_block;
  gsl::not_null<const Function *> entry_point;
  std::unordered_map<NodeID, BlockState> values;
  void build_analysis();
  auto get_join(NodeID blk, ICFG *graph) -> ProgramState;

  // returns if the values changed
  auto execute_block(NodeID blk, ICFG *graph) -> bool;
};

#define D87C2643_53E9_4BB1_836F_78334D9F5BB4
#endif /* D87C2643_53E9_4BB1_836F_78334D9F5BB4 */
